$(document).ready(function () {

  $("#principal").mousedown(function (e) {
    console.log(e);
    console.log(e.screenY);
    console.log(e.clientY);
    console.log(e.button);
  });

  $(document).keydown(function (e) {
    console.log(e.key);
    console.log(e.ctrlKey);
  });

  $("#link1").click(function (e) { 
    e.preventDefault();
    //$("#principal aside p:first").fadeToggle(1000);
    $("#principal aside p:first").fadeOut(1000, function () {
      $("#principal aside p.no-first").slideUp(1000);
    });
  });

  $("#link3").click(function (e) { 
    e.preventDefault();
    $("#principal aside p:first").fadeIn(1000, function () {
      $("#principal aside p.no-first").slideDown(1000);
    });
    //$("#principal aside p").slideToggle(1000);
  });

  $("#link4").click(function (e) { 
    e.preventDefault();
    $("#principal aside p.no-first").animate(
      {
        "font-size": '2em',
        "opacity": "0.3"
     }, 1000, function () {
      $("#principal aside p.no-first").animate(
        {
          "font-size": '1.1em',
          "opacity": "1"
       }, 1000);
     });
  });

});
