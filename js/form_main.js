$(document).ready(function () {
  cleanFrm();

  $("#frm_lab").submit(function (e) {
    if ($("#nombreInput").val() == "") {
      e.preventDefault();
      $("#nombreInput").addClass("is-invalid");
      $("#invalidNombreInput").css("display", "block");
    } else {
      $("#nombreInput").removeClass("is-invalid");
      $("#invalidNombreInput").css("display", "none");
    }
    if (
      $("#emailInput").val() == "" ||
      $("#emailInput").val().indexOf("@") == -1
    ) {
      e.preventDefault();
      $("#emailInput").addClass("is-invalid");
      $("#invalidEmailInput").css("display", "block");
    } else {
      $("#emailInput").removeClass("is-invalid");
      $("#invalidEmailInput").css("display", "none");
    }
    if ($("#motivoInput").val() == "") {
      e.preventDefault();
      $("#motivoInput").addClass("is-invalid");
      $("#invalidMotivoInput").css("display", "block");
    } else {
      $("#motivoInput").removeClass("is-invalid");
      $("#invalidMotivoInput").css("display", "none");
    }
  });

  $("#btnReset").click( function(e){
    e.preventDefault();
    console.log("reset form");
    cleanFrm();
    $("#frm_lab")[0].reset();
    
  });

  $($("#nombreInput")).focus(focusInput);
  $($("#emailInput")).focus(focusInput);
  $($("#motivoInput")).focus(focusInput);

  $($("#nombreInput")).blur(blurInput);
  $($("#emailInput")).blur(blurInput);
  $($("#motivoInput")).blur(blurInput);

  function blurInput(e){
    console.log(e.currentTarget);
    var selectorInvDiv = "#invalid"+e.currentTarget.id.charAt(0).toUpperCase()+e.currentTarget.id.substring(1);

    if(e.currentTarget.value == "" || !isEmailCampAndValid(e)){
        $(e.currentTarget).addClass("is-invalid");
        $(selectorInvDiv).css("display", "block");
    }else{
        $(e.currentTarget).removeClass("is-invalid");
        $(selectorInvDiv).css("display", "none");
    }
  }

  function focusInput(e){
    var selectorInvDiv = "#invalid"+e.currentTarget.id.charAt(0).toUpperCase()+e.currentTarget.id.substring(1);
    $(e.currentTarget).removeClass("is-invalid");
    $(selectorInvDiv).css("display", "none");
  }

  function isEmailCampAndValid(e){
    console.log(e.currentTarget.getAttribute("type"));
    if(e.currentTarget.getAttribute("type") != "email"){
      return true;
    }
    if(e.currentTarget.value.indexOf("@") == -1){
      return false;
    }else{
      return true;
    }
  }

  function cleanFrm() {
    $("#nombreInput").removeClass("is-invalid");
    $("#invalidNombreInput").css("display", "none");
    $("#emailInput").removeClass("is-invalid");
    $("#invalidEmailInput").css("display", "none");
    $("#motivoInput").removeClass("is-invalid");
    $("#invalidMotivoInput").css("display", "none");
  }
});
